//
//  Favorite+CoreDataProperties.swift
//  XYZ Commerce
//
//  Created by Gregorius Albert on 10/02/22.
//
//

import Foundation
import CoreData


extension Favorite {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Favorite> {
        return NSFetchRequest<Favorite>(entityName: "Favorite")
    }
    
    @nonobjc public class func checkItemExist(id:Int) -> NSFetchRequest<Favorite> {
        let fetchRequest = NSFetchRequest<Favorite>(entityName: "Favorite")
        let predicate = NSPredicate(format: "id == %@", "\(id)")
        fetchRequest.predicate = predicate
        return fetchRequest
    }

    @NSManaged public var avgRating: Double
    @NSManaged public var bestComment: String?
    @NSManaged public var bestRating: Double
    @NSManaged public var bestUsername: String?
    @NSManaged public var id: Int64
    @NSManaged public var imageThumb: Data?
    @NSManaged public var price: Int64
    @NSManaged public var stock: Int64
    @NSManaged public var title: String?

}

extension Favorite : Identifiable {

}
