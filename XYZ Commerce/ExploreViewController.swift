//
//  SearchViewController.swift
//  XYZ Commerce
//
//  Created by Gregorius Albert on 07/02/22.
//

import UIKit

class ExploreViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableview: UITableView!
    var arrItems = [Item]()
    var segueItem:Item?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.delegate = self
        tableview.dataSource = self
        searchBar.delegate = self
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longpress(sender:)))
        tableview.addGestureRecognizer(longPress)
        
        //Initial fetch
        fetchAPI(term: "xyz")
    }
    
    func saveHandler(item:Item) -> Void {
        let context = appDelegate.persistentContainer.viewContext
        do{
            let favorites = Favorite(context: context)
            favorites.id = Int64(item.id)
            favorites.title = item.title
            favorites.stock = Int64(item.stock)
            favorites.price = Int64(item.price)
            favorites.bestUsername = item.bestUsername
            favorites.bestRating = item.bestRating
            favorites.bestComment = item.bestComment
            favorites.avgRating = item.avgRating
            
            let png = item.imageThumb?.pngData()
            favorites.imageThumb = png
            
            context.insert(favorites)
            try context.save()
        }
        catch{
            print(error.localizedDescription)
        }
        present(Helper.pushAlert(title: "Save Success", message: "\(item.title) successfully saved to favorites!"), animated: true, completion: nil)
    }
    
    func deleteHandler(item:Item) -> Void {
        let context = appDelegate.persistentContainer.viewContext
        do{
            let fetchRequest = Favorite.checkItemExist(id: item.id)
            let result = try context.fetch(fetchRequest)
            context.delete(result[0])
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
        present(Helper.pushAlert(title: "Delete Success", message: "\(item.title) successfully deleted from favorites!"), animated: true, completion: nil)
    }
    
    
    @objc private func longpress(sender: UILongPressGestureRecognizer) {
        if sender.state == .began {
            let touchPoint = sender.location(in: tableview)
            if let indexPath = tableview.indexPathForRow(at: touchPoint) {
                
                let item = arrItems[indexPath.row]
                
                let sheet = UIAlertController(
                    title: item.title,
                    message: nil ,
                    preferredStyle: .actionSheet
                )
                
                sheet.addAction(UIAlertAction(
                    title: "Cancel",
                    style: .cancel,
                    handler: nil
                ))
                
                let context = appDelegate.persistentContainer.viewContext
                do{
                    let fetchRequest = Favorite.checkItemExist(id: item.id)
                    let result = try context.fetch(fetchRequest)
                    if result.isEmpty{
                        sheet.addAction(UIAlertAction(
                            title: "Save to favorites",
                            style: .default,
                            handler: { action in
                                self.saveHandler(item: item)
                            }
                        ))
                    } else {
                        sheet.addAction(UIAlertAction(
                            title: "Delete from favorites",
                            style: .destructive,
                            handler: { action in
                                self.deleteHandler(item: item)
                            }
                        ))
                    }
                }
                catch{
                    print(error.localizedDescription)
                }
                
                
                
                present(sheet, animated: true, completion: nil)
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let term = searchBar.text!
        
        if term.count < 3 {
            present(Helper.pushAlert(title: "Oops!", message: "Search term must be 3 characters or more!"), animated: true, completion: nil)
        }
        else {
            fetchAPI(term: term.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)
            tableview.reloadData()
        }
        self.view.endEditing(true)
    }
    
    func fetchAPI(term:String) -> Void {
        let urlString = "\(Helper.itemAPI)\(term)"
        let url = URL(string: urlString)!
        
        arrItems.removeAll()
        
        let req = URLRequest(url: url)
        let session = URLSession.shared
        let task = session.dataTask(with: req) { data, res, error in
            if error == nil {
                do {
                    let root = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String:Any]
                    
                    let response = res as? HTTPURLResponse
                    let statusCode = response?.statusCode
                    
                    // Validate if items exist in API
                    if statusCode == 200 {
                        let results = root["results"] as! [[String:Any]]
                        
                        // Count average rating
                        for result in results {
                            var total = 0.0
                            var count = 0.0
                            var bestRating:Double? = 0.0
                            var bestUsername:String? = ""
                            var bestComment:String? = ""
                            
                            let reviews = result["reviews"] as! [[String:Any]]
                            for review in reviews {
                                total += review["score"] as! Double
                                count += 1
                                var max = 0.0
                                
                                if review["score"] as! Double > max {
                                    max = review["score"] as! Double
                                    bestRating = max
                                    bestUsername = review["username"] as? String
                                    bestComment = review["comment"] as? String
//                                    print(max)
//                                    print(review["username"])
                                }
                                
                            }
                            
                            var avg:Double = 0.0
                            
                            if total != 0{
                                avg = total / count
                            }
                            
                            let item = Item(
                                id: result["product_id"] as! Int,
                                title: result["product_title"] as! String,
                                price: result["price"] as! Int,
                                stock: result["stock"] as! Int,
                                imageUrl: "\(Helper.imageAPI)\(result["image"]!)",
                                avgRating: avg,
                                bestRating: bestRating ?? 0.0,
                                bestUsername: bestUsername ?? "",
                                bestComment: bestComment ?? ""
                            )
                            
                            let row = self.arrItems.count
                            self.arrItems.append(item)
                            
                            let imageURL = URL(string: item.imageUrl)!
                            let imageTask = session.dataTask(with: imageURL) { data, response, error in
                                
                                if error == nil {
                                    self.arrItems[row].imageThumb = UIImage(data: data!)
                                    let indexPath = IndexPath(row: row, section: 0)
                                    DispatchQueue.main.async {
                                        self.tableview.reloadRows(at: [indexPath], with: .fade)
                                    }
                                }
                                
                            }
                            imageTask.resume()
                        }
                        DispatchQueue.main.async {
                            self.tableview.reloadData()
                        }
                    }
                    else {
                        print("Textfield Empty")
                    }
                }
                catch {
                    print(error.localizedDescription)
                }
            }
            else {
                print(error!.localizedDescription)
            }
        }
        task.resume()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        let item = arrItems[indexPath.row]
        
        var avgStr = "-"
        if item.avgRating > 0 {
            avgStr = "\(String(format: "%.1f", item.avgRating))"
        }
        
        cell?.textLabel!.text = item.title
        cell?.detailTextLabel!.text = "Rp. \(item.price) (Rating: \(avgStr))"
        cell?.imageView?.image = arrItems[indexPath.row].imageThumb
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        segueItem = arrItems[indexPath.row]
        performSegue(withIdentifier: "exploreToDetailSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "exploreToDetailSegue" {
            let destination = segue.destination as! DetailViewController
            destination.item = segueItem
        }
    }
    



}
