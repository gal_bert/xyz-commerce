//
//  FavoritesViewController.swift
//  XYZ Commerce
//
//  Created by Gregorius Albert on 07/02/22.
//

import UIKit

class FavoritesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
      
    @IBOutlet weak var tableview: UITableView!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate    
    var arrItems = [Favorite]()
    var segueItem:Item?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.delegate = self
        tableview.dataSource = self
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longpress(sender:)))
        tableview.addGestureRecognizer(longPress)

        loadItems()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadItems()
    }
    
    func loadItems() -> Void {
        let context = appDelegate.persistentContainer.viewContext
        do{
            let fetchRequest = Favorite.fetchRequest()
            let result = try context.fetch(fetchRequest)
            arrItems = result
        }
        catch{
            print(error.localizedDescription)
        }
        tableview.reloadData()
    }
    
    func deleteHandler(item:Favorite) -> Void {
        let context = appDelegate.persistentContainer.viewContext
        var temp:String?
        do{
            let fetchRequest = Favorite.checkItemExist(id: Int(item.id))
            let result = try context.fetch(fetchRequest)
            temp = result[0].title
            context.delete(result[0])
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
        loadItems()
        present(Helper.pushAlert(title: "Delete Success", message: "\(temp!) successfully deleted from favorites!"), animated: true, completion: nil)
    }
    
    @objc private func longpress(sender: UILongPressGestureRecognizer) {
        if sender.state == .began {
            let touchPoint = sender.location(in: tableview)
            if let indexPath = tableview.indexPathForRow(at: touchPoint) {
                
                let item = arrItems[indexPath.row]
                
                let sheet = UIAlertController(
                    title: item.title,
                    message: nil ,
                    preferredStyle: .actionSheet
                )
                
                sheet.addAction(UIAlertAction(
                    title: "Cancel",
                    style: .cancel,
                    handler: nil
                ))
                
                sheet.addAction(UIAlertAction(
                    title: "Delete from favorites",
                    style: .destructive,
                    handler: { action in
                        self.deleteHandler(item: item)
                    }
                ))
                
                
                present(sheet, animated: true, completion: nil)
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        let item = arrItems[indexPath.row]
        cell?.textLabel!.text = item.title
        
        var tempStr:String?
        if item.avgRating == 0 {
            tempStr = "-"
        } else {
            tempStr = "\(item.avgRating)"
        }
        cell?.detailTextLabel!.text = "Rp. \(item.price) (Rating: \(tempStr!))"
        
        let image = UIImage(data: item.imageThumb!)
        cell?.imageView!.image = image!
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = arrItems[indexPath.row]
        segueItem = Item(
            id: Int(item.id),
            title: item.title!,
            price: Int(item.price),
            stock: Int(item.stock),
            imageUrl: "",
            imageThumb: UIImage(data: item.imageThumb!)
        )
        performSegue(withIdentifier: "favoritesToDetailSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "favoritesToDetailSegue" {
            let destination = segue.destination as! DetailViewController
            destination.item = segueItem
        }
    }

}
