//
//  Item.swift
//  XYZ Commerce
//
//  Created by Gregorius Albert on 08/02/22.
//

import Foundation
import UIKit

struct Item {
    var id:Int
    var title:String
    var price:Int
    var stock:Int
    var imageUrl:String
    var imageThumb:UIImage? = nil
    var avgRating:Double = 0.0
    var bestRating:Double = 0.0
    var bestUsername:String = ""
    var bestComment:String = ""
}
