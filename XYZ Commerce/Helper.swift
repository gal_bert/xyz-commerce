//
//  Helper.swift
//  XYZ Commerce
//
//  Created by Gregorius Albert on 08/02/22.
//

import Foundation
import UIKit

class Helper {
    static let itemAPI = "https://gz4ad4m977.execute-api.ap-southeast-1.amazonaws.com/get-products?keyword="
    static let imageAPI = "https://thumbnails2022113.s3.ap-southeast-1.amazonaws.com/"
    
    static func pushAlert(title:String, message:String) -> UIAlertController{
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(
            title: "OK",
            style: .default,
            handler: nil
        ))
        
        return alert
    }
    
    
        
}

