//
//  DetailViewController.swift
//  XYZ Commerce
//
//  Created by Gregorius Albert on 07/02/22.
//

import UIKit

class DetailViewController: UIViewController {
        
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var stockLabel: UILabel!
    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var button: UIButton!
    
    var item:Item?
    var itemExist:Bool?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate


    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.image = item?.imageThumb
        titleLabel.text = "Title: \(item!.title)"
        priceLabel.text = "Price: \(item!.price)"
        stockLabel.text = "Stock: \(item!.stock)"
        if item?.bestUsername != "" {
            reviewLabel.text = "\(item!.bestUsername) - Rating: \(item!.bestRating)"
            commentLabel.text = "Comment: \(item!.bestComment)"
        }
        else {
            reviewLabel.text = "No Reviews!"
            commentLabel.text = ""
        }
        validateItemExistence()
    }
    
    func validateItemExistence() -> Void {
        let context = appDelegate.persistentContainer.viewContext
        
        do{
            let fetchRequest = Favorite.checkItemExist(id: item!.id)
            let result = try context.fetch(fetchRequest)
            if result.isEmpty{
                itemExist = false
                button.tintColor = UIColor.systemBlue
                button.setTitle("Save", for: .normal)
            } else {
                itemExist = true
                button.tintColor = UIColor.red
                button.setTitle("Delete", for: .normal)
            }
        }
        catch{
            print(error.localizedDescription)
        }
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionButton(_ sender: Any) {
        
        let context = appDelegate.persistentContainer.viewContext
       
        if itemExist!{
            do{
                let fetchRequest = Favorite.checkItemExist(id: item!.id)
                let result = try context.fetch(fetchRequest)
                context.delete(result[0])
                try context.save()
            } catch {
                print(error.localizedDescription)
            }
            present(Helper.pushAlert(title: "Success!", message: "\(item!.title) deleted from favorites!"), animated: true, completion: nil)
            validateItemExistence()
        }
        else {
            do{
                let favorites = Favorite(context: context)
                favorites.id = Int64(item!.id)
                favorites.title = item!.title
                favorites.stock = Int64(item!.stock)
                favorites.price = Int64(item!.price)
                favorites.bestUsername = item!.bestUsername
                favorites.bestRating = item!.bestRating
                favorites.bestComment = item!.bestComment
                favorites.avgRating = item!.avgRating
                
                let png = item!.imageThumb?.pngData()
                favorites.imageThumb = png
                
                context.insert(favorites)
                try context.save()
                
                present(Helper.pushAlert(title: "Success!", message: "\(item!.title) saved to favorites!"), animated: true, completion: nil)
                validateItemExistence()
            }
            catch {
                print(error.localizedDescription)
            }
        }
        
        
    }
    
}
